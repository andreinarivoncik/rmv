#!/usr/bin/python
# ! -*- coding: utf-8 -*-

from setuptools import setup, find_packages
from os.path import join, dirname
from rm import __version__
from rm import __author__


setup(
    name='rmv',
    author=__author__,
    author_email="andrejnarivoncik@gmail.com",
    platforms="UNIX",
    version=__version__,
    packages=find_packages(),
    long_description=open(join(dirname(__file__), 'README.txt')).read(),
    entry_points={
        'console_scripts':
            ['rmv = rm.main:main']
        },
    test_suite='unittests'
)
