#!/usr/bin/python
# ! -*- coding: utf-8 -*-

import os
from datetime import datetime
from rm_functions import get_size
from abc import abstractmethod


class CleaningPolicy:
    def __init__(self):
        pass

    @abstractmethod
    def get_paths_for_cleaning(self):
        pass


class FullCleaning(CleaningPolicy):
    def __init__(self, basket_path):
        CleaningPolicy.__init__(self)
        self.basket_path = basket_path

    def get_paths_for_cleaning(self):
        removing_list = list()
        for dir_name in os.listdir(self.basket_path):
            removing_list.append(os.path.join(self.basket_path, dir_name))
        return removing_list


class TimeInterval(CleaningPolicy):
    def __init__(self, basket_path, time_interval):
        CleaningPolicy.__init__(self)
        self.basket_path = basket_path
        self.time_interval = time_interval

    def get_paths_for_cleaning(self):
        removing_list = list()
        for dir_name in os.listdir(self.basket_path):
            removing_data = datetime.fromtimestamp(os.stat(os.path.join(self.basket_path, dir_name)).st_mtime)
            if (datetime.today() - removing_data).days >= self.time_interval:
                removing_list.append(os.path.join(self.basket_path, dir_name))
        return removing_list


class MaxAmount(CleaningPolicy):
    def __init__(self, basket_path, amount):
        CleaningPolicy.__init__(self)
        self.basket_path = basket_path
        self.amount = amount

    def get_paths_for_cleaning(self):
        removing_list = list()
        for dir_name in os.listdir(self.basket_path):
            removing_data = get_size(os.path.join(self.basket_path, dir_name))
            if removing_data >= self.amount:
                removing_list.append(os.path.join(self.basket_path, dir_name))
        return removing_list


class MixCleaning(CleaningPolicy):
    def __init__(self, basket_path, amount, time_interval):
        CleaningPolicy.__init__(self)
        self.basket_path = basket_path
        self.amount = amount
        self.time_interval = time_interval

    def get_paths_for_cleaning(self):
        removing_list = list()
        for dir_name in os.listdir(self.basket_path):
            removing_time_data = datetime.fromtimestamp(os.stat(os.path.join(self.basket_path, dir_name)).st_mtime)
            removing_volume_data = get_size(os.path.join(self.basket_path, dir_name))
            if (datetime.today() - removing_time_data).days >= self.time_interval\
                    and removing_volume_data >= self.amount:
                removing_list.append(os.path.join(self.basket_path, dir_name))

        return removing_list


def get_cleaning_policy(policy, basket_path, days, dir_amount):
    if policy == "mix_cleaning":
        return MixCleaning(basket_path, dir_amount, days)
    elif policy == "time_interval":
        return TimeInterval(basket_path, days)
    elif policy == "max_amount":
        return MaxAmount(basket_path, dir_amount)
    else:
        return FullCleaning(basket_path)
