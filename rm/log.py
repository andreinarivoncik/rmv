import logging


def create_logger(name, level, file_log_format, log_format, silent, filename=None, filemode='a'):
    logger = logging.getLogger(name)
    logger.setLevel(level)
    formatter = logging.Formatter(log_format)
    file_formatter = logging.Formatter(file_log_format)

    ch = logging.StreamHandler()
    ch.setLevel(level)
    ch.setFormatter(formatter)

    fh = logging.FileHandler(filename=filename, mode=filemode)
    fh.setLevel(level)
    fh.setFormatter(file_formatter)

    fh_fil = logging.Filter()
    fh_fil.filter = lambda(rec): rec.levelno in (logging.DEBUG, logging.ERROR, logging.CRITICAL)
    fh.addFilter(fh_fil)

    ch_fil = logging.Filter()
    if not silent:
        ch_fil.filter = lambda (rec): rec.levelno in (logging.INFO, logging.WARNING,
                                                      logging.ERROR, logging.CRITICAL)
    else:
        ch_fil.filter = lambda (rec): rec.levelno == logging.CRITICAL
    ch.addFilter(ch_fil)

    logger.addHandler(fh)
    logger.addHandler(ch)


def get_logger(logger_name):
    logger = logging.getLogger(logger_name)
    return logger
