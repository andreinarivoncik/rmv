#!/usr/bin/env python
# coding:utf-8

import os
import json
import shutil
from re import match


def create_folder(path):
    os.mkdir(path)


def get_size(start_path):
    total_size = 0
    total_size += os.path.getsize(start_path)
    for dir_path, dir_names, file_names in os.walk(start_path):
        for f in file_names:
            fp = os.path.join(dir_path, f)
            if not os.path.islink(fp):
                total_size += os.path.getsize(fp)
        for d in dir_names:
            dp = os.path.join(dir_path, d)
            if not os.path.islink(dp):
                total_size += os.path.getsize(dp)
    return total_size


def get_count_files_and_directories(start_path):
    count = 0
    for dir_path, dir_names, file_names in os.walk(start_path):
        count += len(dir_names) + len(file_names)
    count += 1
    return count


def full_basket_cleaning(path):
    count = 0
    for DIR in os.listdir(path):
        path_dir = os.path.join(path, DIR)
        if os.path.isfile(path_dir):
            os.remove(path_dir)
            count += 1
        elif os.path.isdir(path_dir) and\
                os.path.islink(path_dir):
            os.remove(path_dir)
        else:
            count += delete_dirs(path_dir, 0)
    return count


def clean_dir(path):
    count = 0
    if os.path.isfile(path) or os.path.islink(path):
        os.remove(path)
        count += 1
        return count
    for DIR in os.listdir(path):
        path_dir = os.path.join(path, DIR)
        if os.path.isfile(path_dir):
            os.remove(path_dir)
            count += 1
        elif os.path.isdir(path_dir) and\
                os.path.islink(path_dir):
            os.remove(path_dir)
        else:
            count += delete_dirs(path_dir, 0)
    os.rmdir(path)
    count += 1
    return count


def delete_dirs(direct, count):
    for name in os.listdir(direct):
        file_path = os.path.join(direct, name)
        if os.path.exists(file_path):
            if not os.path.islink(file_path) and os.path.isdir(file_path):
                delete_dirs(file_path, count)
            else:
                os.remove(file_path)
                count += 1
    os.rmdir(direct)
    count += 1
    return count


def remove(old_path, path_in_basket, remove_not_empty=False):
    if os.path.isdir(old_path):
        if len(os.listdir(old_path)) and remove_not_empty:
            return
        else:
            os.renames(old_path, path_in_basket)
    else:
        os.renames(old_path, path_in_basket)


def check_for_new_name(path_in_basket):
    exists = True
    counter = 0
    while exists:
        if os.path.exists(path_in_basket):
            counter += 1
            if counter > 1:
                new_path = path_in_basket.replace("({})".format(str(counter-1)), "({})".format(str(counter)))
                path_in_basket = new_path
            else:
                path_in_basket += "({})".format(str(counter))
        else:
            directory, name = os.path.split(path_in_basket)
            return name


def select_for_regex_removing(path, regex, removed_paths=list()):
    for name in os.listdir(path):
        file_or_dir_path = os.path.join(path, name)
        if not os.path.islink(file_or_dir_path) and os.path.isdir(file_or_dir_path):
            select_for_regex_removing(file_or_dir_path, regex, removed_paths)
            if match(regex, name):
                removed_paths.append(file_or_dir_path)
        else:
            if match(regex, name):
                removed_paths.append(file_or_dir_path)
    return removed_paths


def select_for_recurs_removing(path, dr='', removed_paths=list()):
    if os.path.isfile(path) or os.path.islink(path):
        removed_paths.append(dr)
    else:
        for name in os.listdir(path):
            file_or_directory = os.path.join(path, name)
            select_for_recurs_removing(file_or_directory, os.path.join(dr, name), removed_paths)
        if dr != '':
            removed_paths.append(dr)
    return removed_paths


def recovery(cur_path, old_path):
    os.renames(cur_path, old_path)


def fast_basket_cleaning(path):
    shutil.rmtree(path, ignore_errors=True)
    os.mkdir(path)


def read_info_json(json_file):
    with open(json_file, 'r') as f:
        json_object = json.load(f)
        return json_object


def write_info_json(json_file, json_object):
    with open(json_file, 'w') as f:
        json.dump(json_object, f, indent=4)


def read_info_user(user_file):
    with open(user_file) as f:
        return f.readlines()


def write_info_user(user_file, lines_list):
    with open(user_file, 'w') as wf:
        wf.writelines(lines_list)


def append_info_user(user_file, st):
    with open(user_file, 'a') as f:
        f.writelines(st)


if __name__ == "__main__":
    pass
    # shutil.move("/home/andrei/qq", "/home/andrei/basket/folder/lol")
    # remove("/home/andrei/1", "/home/andrei/basket")

    # os.rename("/home/andrei/qqq", "/home/andrei/basket/123")
    # os.renames("/home/andrei/qqq", "/home/andrei/basket/123")

    # os.link(dst, "/home/andrei/llllll.txt")
    # pass
    # print os.path.expandvars("../main.py")
    # print os.path.split(os.path.abspath("../../main.py"))
    # print os.path.getatime("main.py")
    # print os.path.getmtime(os.path.abspath("main.py"))
    # print os.getcwd()
    # os.open("lol.txt", 'r')
    # os.mkdir('TEXT')#создать каталог, если уже существует OSError
    # os.makedirs("lol/lol/lol")#создать дерево каталогов
    # print os.path.exists("main.py")#существует ли путь
    # print os.path.dirname("~/rm.egg-info/top_level.txt")#Отбрасывает имя последнего файлав пути
    # print os.path.curdir
    # x = os.listdir(os.path.curdir)#Список папок и файлов директории
    # print x
    # print os.listdir(os.path.curdir + "/" + x[5])
    # print os.stat('main.py').st_size#получить информацию о файле или директории
    # os.removedirs("lol/lol/lol")#удалить все каталоги по пути
    # os.remove("lol.txt")
    # os.renames("lol/lol/lol", "kek/kek/kek")#переименовать дерево в новое дерево
    # shutil.copytree("kek", "lol1")#копировать дерево kek в директорию lol1
    # shutil.copy("main.py", "main1.py")#копировать файл
    # shutil.move("lol", "lol1")#переместить дерево в другое место
    # print shutil.rmtree("lol1", onerror=lambda x :10)
