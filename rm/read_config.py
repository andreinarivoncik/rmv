#!/usr/bin/python
# ! -*- coding: utf-8 -*-

from conf_files_work import read_config_json_file
from log import get_logger
import os


def read_configuration(conf_setting_list, default_conf):
    configs = read_config_json_file(default_conf)
    if conf_setting_list is not None:
        configs = _change_configuration(conf_setting_list, configs)
    return configs


def _change_configuration(conf_settings_list, conf_obj):
    logger = get_logger("logger")
    for setting in conf_settings_list:
        try:
            k, v = setting.split('=')
        except ValueError:
            logger.warning("undefined argument %s", setting)
        else:
            if k == "path":
                if os.path.exists(os.path.expanduser(v)):
                    conf_obj[k] = v
                else:
                    logger.info("the basket path did not changed!")
            elif k == "cleaning_policy":
                if v in ("max_amount", "time_interval", "mix_cleaning", "full_cleaning"):
                    conf_obj[k] = v
                else:
                    logger.info("wrong cleaning_politic!")
            else:
                try:
                    val = int(v)
                except ValueError:
                    logger.error("wrong setting: %s", setting)
                else:
                    if k == "view_lines_count" and 0 < val < 40:
                        conf_obj[k] = val
                    if k == "max_basket_volume":
                        if val <= 0:
                            logger.info("wrong max_basket_volume!")
                        else:
                            conf_obj[k] = val
                    if k == "days" and val > 0:
                        conf_obj[k] = val
                    if k == "dir_amount":
                        if 0 < val < conf_obj["max_basket_volume"]:
                            conf_obj[k] = val
                        else:
                            logger.info("volume: wrong argument!")
    return conf_obj
