#!/usr/bin/env python
# coding:utf-8

from rm_functions import *
from cleaning_policy import *
from datetime import date
from log import get_logger
from os.path import join
from os.path import split
from os.path import exists
import datetime


def ask_user(mess, arg_i, for_all_flag=None):
    if not arg_i:
        return True
    if for_all_flag is None:
        inp = raw_input(mess + " [Y/N]")
        if inp == 'Y':
            return True
        else:
            return False
    if for_all_flag is not None:
        if for_all_flag["for_all_flag"] == "Y*":
            return True
        elif for_all_flag["for_all_flag"] == "N*":
            return False
        else:
            inp = raw_input(mess + " [Y/N/Y*/N*]")
            if inp == "Y*":
                for_all_flag["for_all_flag"] = "Y*"
                return True
            if inp == "N*":
                for_all_flag["for_all_flag"] = "N*"
                return False
            if inp == 'Y':
                return True
            else:
                return False


def _is_dir_exists(path):
    if not exists(path):
        return False
    else:
        return True


def _check_free_space(path, cur_basket_volume, max_size):
    if get_size(path) + cur_basket_volume > max_size:
        return False
    else:
        return True


def _common_removing(remove_path, in_basket_path, dry_run, arg_i, remove_not_empty=False, for_all_flag=None):
    logger = get_logger("logger")
    if not os.access(remove_path, os.W_OK):
        logger.warning("%s: attempt to delete from protected file or directory!", remove_path)
    if ask_user(u"Do you want to remove {}?".format(remove_path), arg_i, for_all_flag=for_all_flag):
        if not dry_run:
            try:
                remove(remove_path, in_basket_path, remove_not_empty)
            except OSError as ex:
                logger.error("%s: " + ex.message, remove_path)
                return False
            else:
                logger.info(u"%s: was successfully deleted!", remove_path)
                return True
        else:
            logger.info(u"%s: was successfully deleted!", remove_path)
            return True
    else:
        logger.info("{}: the operation was successfully canceled!".format(remove_path))
        return False


def remove_control(mode_rm, path, basket, arg_i, dry_run, max_basket_volume, regex=None):
    for_all_flag = {"for_all_flag": False}
    if not _is_dir_exists(basket.path):
        raise UserWarning("{}: file or directory not found!".format(basket.path))
    if not _is_dir_exists(path):
        raise UserWarning("{}: file or directory not found!".format(path))
    if not _check_free_space(path, basket.size, max_basket_volume):
        raise UserWarning("Memory limit exceeded. Increase the size of the basket or clean it.")

    size = 0
    count = 0
    content = basket.content
    if mode_rm == "del":
        dir_path, dir_name = split(path)
        new_name = check_for_new_name(basket.path_in_basket(dir_name))
        path_in_basket_new_name = basket.path_in_basket(new_name)
        is_del = _common_removing(path, path_in_basket_new_name, dry_run, arg_i)
        if not dry_run and is_del:
            size += get_size(basket.path_in_basket(new_name))
            count += get_count_files_and_directories(path_in_basket_new_name)
            content[new_name] = path

    if mode_rm == "reg":
        if os.path.islink(path) or not os.path.isdir(path):
            raise UserWarning("{}: not directory!".format(path))
        selected_paths = select_for_regex_removing(path, regex)
        new_names_and_old_paths = dict()
        for pth in selected_paths:
            dir_path, dir_name = split(pth)
            new_name = check_for_new_name(basket.path_in_basket(dir_name))
            path_in_basket_new_name = basket.path_in_basket(new_name)
            is_del = _common_removing(pth, path_in_basket_new_name, dry_run, arg_i, for_all_flag=for_all_flag)
            if not dry_run and is_del:
                size += get_size(path_in_basket_new_name)
                count += get_count_files_and_directories(path_in_basket_new_name)
                new_names_and_old_paths[new_name] = pth
        content.update(new_names_and_old_paths)

    if mode_rm == "recurs":
        if os.path.islink(path) or not os.path.isdir(path):
            raise UserWarning(u"{}: file can not be removed!".format(path))
        selected_paths = select_for_recurs_removing(path)
        dir_path, dir_name = split(path)
        new_name_in_basket = check_for_new_name(basket.path_in_basket(dir_name))
        new_path = join(dir_path, new_name_in_basket)
        os.rename(path, new_path)
        for pth in selected_paths:
            from_dir_path = join(new_path, pth)
            in_dir_path = join(basket.path_in_basket(new_name_in_basket), pth)
            is_del = exists(from_dir_path)
            if is_del:
                is_del = _common_removing(from_dir_path, in_dir_path, dry_run, arg_i, True, for_all_flag)
            if not dry_run and is_del and exists(in_dir_path):
                size += get_size(in_dir_path)
                count += get_count_files_and_directories(in_dir_path)
        content[new_name_in_basket] = path

        if exists(new_path):
            os.rename(new_path, path)
    basket.add_size_and_filecount(size, count)
    basket.content = content


def recovery_control(name, basket, dry_run, arg_i):
    logger = get_logger("logger")
    content = basket.content
    path_in_basket = basket.path_in_basket(name)
    try:
        old_path = content[name]
    except KeyError:
        raise UserWarning("{}: file or directory not found!".format(basket.path_in_basket(name)))
    if old_path:
        if exists(old_path):
            logger.warning(u"%s: file or directory with that name already exists!", old_path)
            if not ask_user("Replace file?", arg_i):
                while exists(old_path):
                    old_path += ' new'
            else:
                if not dry_run:
                    if os.path.isdir(old_path) or os.path.islink(old_path):
                        shutil.rmtree(old_path)
                    else:
                        os.remove(old_path)
                logger.info("file %s successfully changed!", old_path)
        if not dry_run and exists(old_path):
            content.pop(name)
            recovery(path_in_basket, old_path)
            size = get_size(old_path)
            files_count = get_count_files_and_directories(old_path)
            basket.sub_size_and_filecount(size, files_count)
        logger.info("file %s was successfully recovered!", name)
        basket.content = content


def basket_cleaning_control(basket, cleaning_policy, dry_run, arg_i):
    logger = get_logger("logger")
    content = basket.content
    if ask_user("Do you want to clear a basket?", arg_i):
        count = 0
        size = 0
        removing_list = cleaning_policy.get_paths_for_cleaning()
        for name in removing_list:
            if not dry_run and exists(name):
                try:
                    size += get_size(name)
                    count += clean_dir(name)
                    content.pop(name)
                except KeyError:
                    pass
        if not dry_run:
            basket.last_cleaning = str(date.today())
            basket.sub_size_and_filecount(size, count)
        logger.info("Basket cleared successfully! Was cleared %s kB.", size)
        basket.content = content
    else:
        logger.info("The operation was successfully canceled!")


def view_basket(basket, args_i, lines_count):
    logger = get_logger("logger")
    for_all_flag = {"for_all_flag": False}
    if not ask_user("Do you want to view basket?", args_i):
        logger.info("The view operation was successfully canceled!")
        return
    count = lines_count

    for f in os.listdir(basket.path):
        try:
            logger.info(u"Name: {name},\tvolume: {volume},\tremoved date: {date}".
                        format(name=f, volume=os.stat(basket.path_in_basket(f)).st_size,
                               date=datetime.datetime.fromtimestamp(os.stat(basket.path_in_basket(f)).st_mtime)))
            count -= 1
        except OSError as er:
            logger.warning(u"{name}: {message} {text}".format(name=f, message=er.strerror, text="Corrupted link!"))
        if count == 0:
            if ask_user("can you give next files?", args_i, for_all_flag):
                count = lines_count
            else:
                break
    logger.info("Successful basket view!")
