import re
import os


class Basket:
    def __init__(self, path, size, files_count, last_cleaning, content):
        self._path = os.path.join(path, "basket")
        self._size = size
        self._files_count = files_count
        self._last_cleaning = last_cleaning
        self._content = content

    def __str__(self):
        str_ = "absolute path: {0}\nsize: {1}\nfiles count: {2}\ndate of last cleaning: {3}".format(
            self._path, self._size, self._files_count, self._last_cleaning)
        return str_

    @property
    def path(self):
        return self._path

    @property
    def size(self):
        return self._size

    @property
    def files_count(self):
        return self._files_count

    @property
    def last_cleaning(self):
        return self._last_cleaning

    @last_cleaning.setter
    def last_cleaning(self, value):
        self._last_cleaning = value

    @property
    def content(self):
        return self._content

    @content.setter
    def content(self, value):
        self._content = value

    def sub_size_and_filecount(self, files_size, files_count):
        self._size -= files_size
        self._files_count -= files_count

    def add_size_and_filecount(self, files_size, files_count):
        self._size += files_size
        self._files_count += files_count

    def path_in_basket(self, name):
        return os.path.join(self._path, name)

    def check_basket_and_file_in_basket(self, path):
        if self._path == path:
            return False
        if re.search(self._path, path):
            return False
        return True

    @staticmethod
    def file_name_in_basket(path):
        return os.path.split(path)[1]
