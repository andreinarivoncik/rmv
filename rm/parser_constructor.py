#!/usr/bin/python
# ! -*- coding: utf-8 -*-

import argparse


def pars_init():
    parser = argparse.ArgumentParser(description="Console utility options %(prog)s",
                                     epilog="All program options %(prog)s", prog="rmv")
    parser.add_argument("--i", "-i", action='store_const', const=1, default=0,
                        help="Request to confirm each delete operation.")
    parser.add_argument("--f", "-f", action='store_const', const=1, default=0,
                        help="Ignore error in function.")
    parser.add_argument("--dry_run", "-y", action='store_const', const=1, default=0,
                        help="Simulation of operations, changes do not occur.")
    parser.add_argument("--silent", "-s", action='store_const', const=1, default=0,
                        help='Silent mode.')
    parser.add_argument("--clear", "-c", action='store_const', const=1, default=0,
                        help='Clear basket.')
    parser.add_argument("--view", "-w", action='store_const', const=1, default=0,
                        help='Show basket.')
    parser.add_argument("--upd_conf", "-u", action='store_const', const=1, default=0,
                        help='Update configuration file.')
    parser.add_argument("--path", "-", nargs='*', default=list(), help="paths list.")
    parser.add_argument("--config", "-cf", nargs='*',
                        help="List of optional parameters of the configuration file. "
                             "cleaning_policy=[mix_cleaning, full_cleaning, max_amount, time_interval]\n"
                             "max_basket_volume=int_value(kB)\n"
                             "days=int_value\n"
                             "view_lines_count=int_value(1,40)\n"
                             "dir_amount=int_value(kB)\n"
                             "configuration=[conf1, conf2]")
    parser.add_argument("--recurs", "-r", action='store_const', const=1, default=0,
                        help="Processing of all sub-folders, if the request to delete the directory.")
    parser.add_argument("--del", "-d", action='store_const', const=1, default=0,
                        help="Removing a file or directory by a given path.")
    parser.add_argument("--reg", "-rg", action='store', type=str,
                        help="Removing files by specified path and regular expression.")
    parser.add_argument("--rec", "-rc", nargs='*', default=list(), type=str,
                        help="The name of the restored directory or file")
    parser.add_argument("--version", "-v", action='version', version='%(prog)s 1.0',
                        help="Program version.")
    return parser

