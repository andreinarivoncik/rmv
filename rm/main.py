#!/usr/bin/python
# ! -*- coding: utf-8 -*-

import log
from basket import Basket
from conf_files_work import write_config_info_user
from conf_files_work import read_conf_path
from conf_files_work import write_config_json_file
from conf_files_work import convert_from_json_in_user_format
from parser_constructor import pars_init
from read_config import *
from control import *
from rm_functions import *


def create_loggers(log_file, silent):
    log.create_logger("logger", log.logging.DEBUG,
                      file_log_format=u'%(filename)s[LINE:%(lineno)d]# %(levelname)-5s [%(asctime)s]  %(message)s',
                      log_format=u'%(levelname)-5s %(message)s', silent=silent, filemode='a', filename=log_file)


def create_basket(basket_path, other_info_files, basket_content_file):
    logger = get_logger("logger")
    basket_content = read_info_json(basket_content_file)
    if not exists(basket_path):
        create_folder(basket_path)
        logger.info("basket was successfully created!")
        basket = Basket(basket_path, 0, 0, str(date.today()), basket_content)
    else:
        info = read_info_json(other_info_files)
        basket = Basket(basket_path, info["cur_baskets_size"], info["cur_files_count"],
                        info["last_cleaning"], basket_content)
    return basket


def show_basket(basket, arg_i, view_lines_count):
    logger = get_logger("logger")
    try:
        view_basket(basket, arg_i, view_lines_count)
    except OSError as er:
        logger.error("baskets showing: %s", er.message)
        return -2
    except Exception as ex:
        logger.error("baskets showing: %s", ex.message)
        return -2
    return 0


def run_removing(basket, args, max_basket_volume):
    logger = get_logger("logger")
    for_all_flag = {"for_all_flag": False}
    for p in args["path"]:
        p = join(os.path.abspath(os.path.curdir), p)
        check_error = basket.check_basket_and_file_in_basket(p)
        if not check_error:
            logger.info("%s: it is a basket or file in basket!")
            return -3
        try:
            if args["del"]:
                    remove_control("del", p, basket, args['i'], args["dry_run"], max_basket_volume)
            elif not ask_user("Do you want to remove file or directory: {}?".format(p), args['i'], for_all_flag):
                logger.info("The removing operation was successfully canceled, NAME: {}!".format(p))
            else:
                if args["recurs"]:
                    remove_control("recurs", p, basket, args['i'], args["dry_run"], max_basket_volume)
                elif args["reg"]:
                    remove_control("reg", p, basket, args['i'], args["dry_run"], max_basket_volume, args["reg"])
        except UserWarning as warn:
            logger.warning("removing: %s", warn.message)
            if not args['f']:
                return -3
        except Exception as ex:
            logger.warning("removing: %s", ex.message)
            if not args['f']:
                return -3
    return 0


def run_basket_cleaning(basket, arg_i, dry_run, configs, logs_file):
    logger = get_logger("logger")
    with open(logs_file, 'w'):
        pass
    clean_policy = get_cleaning_policy(configs["cleaning_policy"], basket.path, configs["days"], configs["dir_amount"])
    try:
        basket_cleaning_control(basket, clean_policy, dry_run, arg_i)
    except OSError as er:
        logger.exception("baskets cleaning: %s", er.message)
        return -4
    except Exception as ex:
        logger.exception("baskets cleaning: %s", ex.message)
        return -4
    return 0


def run_recovery(basket, args):
    logger = get_logger("logger")
    for_all_flag = {"for_all_flag": False}
    for name in args["rec"]:
        try:
            if not ask_user("Do you want to recovery file or directory: {}?".format(name), args['i'], for_all_flag):
                logger.info("The recovery operation was successfully canceled, NAME: {}!".format(name))
            else:
                recovery_control(name, basket, args["dry_run"], args['i'])
        except UserWarning as warn:
            logger.error("baskets recovering: %s", warn.message)
            if not args['f']:
                return -5
        except OSError as er:
            logger.error("baskets recovering: %s", er.message)
            if not args['f']:
                return -5
        except Exception as ex:
            logger.error("baskets recovering: %s", ex.message)
            if not args['f']:
                return -5
        return 0


def update_info(basket, json_basket_content_file, other_info_file):
    logger = get_logger("logger")
    if exists(json_basket_content_file):
        write_info_json(json_basket_content_file, basket.content)
        logger.debug("%s: file was updated!!", json_basket_content_file)
    else:
        logger.debug("%s: file was corrupted or deleted!", json_basket_content_file)
        write_info_json(json_basket_content_file, dict())
    other_info = dict()
    other_info["cur_baskets_size"] = basket.size
    other_info["cur_files_count"] = basket.files_count
    other_info["last_cleaning"] = basket.last_cleaning
    if exists(json_basket_content_file):
        write_info_json(other_info_file, other_info)
        logger.debug("%s: file was updated!!", other_info_file)
    else:
        logger.debug("%s: file was corrupted or deleted!", other_info_file)
        write_info_json(other_info_file, dict())


def write_update_config(configs, json_conf_file_path, user_conf_file_path):
    logger = get_logger("logger")
    try:
        write_config_json_file(json_conf_file_path, configs)
        user_format_configs = convert_from_json_in_user_format(configs)
        write_config_info_user(user_conf_file_path, user_format_configs)
    except OSError as er:
        logger.debug("configuration files writing: %s", er.strerror)
        return -6
    return 0


def main():
    args = vars(pars_init().parse_args())
    projects_files_path = read_conf_path()
    json_conf_file_path = "{}cf/default_JSON_config.json".format(projects_files_path)
    user_conf_file_path = "{}cf/default_user_config.txt".format(projects_files_path)
    logs_file = "{}fi/log.log".format(projects_files_path)
    json_basket_content_file = "{}fi/basket_content.json".format(projects_files_path)
    other_info_file = "{}fi/other_info.json".format(projects_files_path)

    # Change configuration by command line parameters
    silent = args["silent"]
    dry_run = args["dry_run"]
    arg_i = args['i']

    create_loggers(logs_file, silent)
    configs = read_configuration(args["config"], json_conf_file_path)
    res = 0

    # create basket
    basket_path = os.path.expanduser(configs["path"])
    basket = create_basket(basket_path, other_info_file, json_basket_content_file)

    # Cleaning of the basket in case it's time or the user wanted
    if args["clear"]:
        res = run_basket_cleaning(basket, arg_i, dry_run, configs, logs_file)
        if res != 0:
            return res

    # Delete depending on the delete settings
    if args["recurs"] or args["reg"] or args["del"]:
        res = run_removing(basket, args, configs["max_basket_volume"])
        if res != 0:
            return res

    # Recovery files or directories
    if args["rec"]:
        res = run_recovery(basket, args)
        if res != 0:
            return res

    # Show basket
    if args["view"]:
        res = show_basket(basket, arg_i, configs["view_lines_count"])
        if res != 0:
            return res

    # Write configuration changes in file
    if args['upd_conf']:
        res = write_update_config(configs, json_conf_file_path, user_conf_file_path)
        if res != 0:
            return res

    update_info(basket, json_basket_content_file, other_info_file)
    return res

if __name__ == "__main__":
    lg = log.logging.getLogger("logger")
    lg.setLevel(log.logging.INFO)
    lg.info("Process finished with exit code %s", str(main()))
