#!/usr/bin/python
# ! -*- coding: utf-8 -*-

from os.path import *
import logging


def uninstall():
    if exists(join(expanduser('~'), '.bashrc')):
        fr = open(join(expanduser('~'), '.bashrc'), 'r')
        lines = fr.readlines()
        fr.close()
        try:
            lines.remove("alias rmv='python " + (join(abspath(curdir), "rm/main.py") + "'\n"))
            lines.remove((abspath(curdir) + '/' + "\n"))
            fw = open(join(expanduser('~'), '.bashrc'), 'w')
            fw.writelines(lines)
            fw.close()
        except ValueError as er:
            logging.error("Deinstallation error: {}".format(er.message))
            return
        logging.info("Deinstallation completed successfully!")
    else:
        logging.error("Deinstallation error!")


if __name__ == '__main__':
    uninstall()
