from __future__ import absolute_import
import unittest
import os
import rm.rm_functions
from shutil import rmtree
from shutil import move


class RemoveRecursTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        print ("Remove recurs file or directory!")

    def test_select_files_and_directories(self):
        path = "temp"
        os.makedirs("temp/lol/123")
        with open("temp/lol/lol1.txt", 'w'):
            pass
        sel_paths = rm.rm_functions.select_for_recurs_removing(path)
        self.assertTrue("lol/123" in sel_paths, "error by selecting!")
        self.assertTrue("lol/lol1.txt" in sel_paths, "error by selecting!")
        self.assertTrue("lol" in sel_paths, "error by selecting!")
        rmtree("temp/lol")

    def test_change_name_by_removing(self):
        basket_path = "temp/basket"
        if not os.path.exists(basket_path):
            os.mkdir(basket_path)
        path1 = "temp/recurs_f.txt"
        path2 = "temp/recurs_f.txt"
        fd = os.open(path1, os.O_RDWR | os.O_CREAT)
        os.close(fd)
        if os.path.exists(basket_path):
            move(path1, basket_path)
        fd = os.open(path2, os.O_RDWR | os.O_CREAT)
        os.close(fd)
        if os.path.exists(basket_path):
            self.assertEqual(rm.rm_functions.check_for_new_name
                             (os.path.abspath(os.path.join(basket_path, os.path.split(path2)[1]))),
                             "recurs_f.txt(1)", "File was not rename!")
        os.remove(path2)
        if os.path.exists(basket_path):
            rmtree(basket_path)

if __name__ == "__main__":
    unittest.main()
