from __future__ import absolute_import
import unittest
import os
import rm.rm_functions
from shutil import rmtree


class RemoveTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        print ("Remove file or directory!")

    def test_remove_file(self):
        file_path = "temp/testfile8.txt"
        basket_path = "temp/basket"
        if not os.path.exists(basket_path):
            os.mkdir(basket_path)
        with open(file_path, 'w'):
            pass
        if os.path.exists(file_path):
            rm.rm_functions.remove(file_path, basket_path)
            self.assertFalse(os.path.exists(file_path), "file was not remove!")
            self.assertTrue(os.path.exists(os.path.join(basket_path, os.path.split(file_path)[1])),
                            "file was not remove!")
        if os.path.exists(basket_path):
            rmtree(basket_path)

    def test_remove_directory(self):
        basket_path = "temp/basket"
        if not os.path.exists(basket_path):
            os.mkdir(basket_path)
        os.makedirs("temp/lol/123")
        with open("temp/lol/lol1.txt", 'w'):
            pass
        os.mkdir("temp/lol/kek")
        dir_path = "temp/lol"
        if os.path.exists(basket_path):
            rm.rm_functions.remove(dir_path, basket_path)
            self.assertTrue(os.path.exists(os.path.join(basket_path, os.path.split(dir_path)[1])),
                            "directory was not remove!")
        if os.path.exists(basket_path):
            rmtree(basket_path)

    def test_remove_link_file(self):
        basket_path = "temp/basket"
        if not os.path.exists(basket_path):
            os.mkdir(basket_path)
        path = "temp/foo.txt"
        dst = "temp/link_foo6.txt"
        fd = os.open(path, os.O_RDWR | os.O_CREAT)
        os.close(fd)
        if not os.path.exists(dst):
            os.link(path, dst)
        os.remove(path)
        if os.path.exists(basket_path):
            rm.rm_functions.remove(dst, basket_path)
            self.assertTrue(os.path.exists(os.path.join(basket_path, os.path.split(dst)[1])),
                            "link file was not remove!")
        if os.path.exists(basket_path):
            rmtree(basket_path)

    def test_change_name_by_removing(self):
        basket_path = "temp/basket"
        if not os.path.exists(basket_path):
            os.mkdir(basket_path)
        path1 = "temp/foo1.txt"
        path2 = "temp/foo1.txt"
        fd = os.open(path1, os.O_RDWR | os.O_CREAT)
        os.close(fd)
        if os.path.exists(basket_path):
            rm.rm_functions.remove(path1, basket_path)
            self.assertTrue(os.path.exists(os.path.join(basket_path, os.path.split(path1)[1])),
                            "File was not remove!")
        fd = os.open(path2, os.O_RDWR | os.O_CREAT)
        os.close(fd)
        if os.path.exists(basket_path):
            rm.rm_functions.remove(path2, basket_path)
            self.assertTrue(os.path.split(path2 + '(1)')[1] in os.listdir(basket_path), "File was not rename!")
            rmtree(basket_path)

if __name__ == "__main__":
    unittest.main()
