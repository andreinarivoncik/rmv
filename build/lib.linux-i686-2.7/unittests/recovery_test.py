from __future__ import absolute_import
import unittest
from rm.rm_functions import create_folder
import os
import rm.rm_functions


class RecoveryTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        print ("Recovering file or directory!")

    # Attempting to restore an existing file
    def test_recovering_file(self):
        basket_path = "temp/basket"
        if not os.path.exists(basket_path):
            os.mkdir(basket_path)
        file_path = "temp/recovery_file.txt"
        with open(file_path, 'w'):
            pass
        if os.path.exists(file_path):
            rm.rm_functions.remove(file_path, basket_path)
            self.assertTrue(os.path.exists(os.path.join(basket_path, os.path.split(file_path)[1])),
                            "file was not remove!")
            rm.rm_functions.recovery(os.path.join(basket_path, os.path.split(file_path)[1]), file_path)
            self.assertTrue(os.path.exists(file_path), "file was not recovery!")
            os.remove(file_path)

    # An attempt to restore a nonexistent file
    def test_no_exists_file_recovering(self):
        file_path = "temp/basket/Nonexistent_file.txt"
        rec_path = "temp/Nonexistent_file.txt"
        if not os.path.exists(file_path):
            self.assertRaises(OSError, rm.rm_functions.recovery, file_path, rec_path)

    # Attempt to restore the system file
    def test_system_file_recovering(self):
        file_path = "/bin/sleep"
        rec_path = "temp/sleep"
        if not os.path.exists(file_path):
            self.assertEqual(OSError, rm.rm_functions.recovery, file_path, rec_path)


if __name__ == "__main__":
    unittest.main()
