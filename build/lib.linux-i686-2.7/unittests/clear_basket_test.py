from __future__ import absolute_import
import unittest
import os
import rm.rm_functions


class ClearBasketTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        print ("Clear basket")

    def test_clear_basket(self):
        path = "temp/basket"
        if not os.path.exists(path):
            os.mkdir(path)
        os.makedirs("temp/basket/temp/test_folder1")
        os.mkdir("temp/basket/123.txt")
        os.mkdir("temp/basket/321")
        rm.rm_functions.full_basket_cleaning(path)
        self.assertEqual(len(os.listdir(path)), 0, "Basket was not clear!")

if __name__ == "__main__":
    unittest.main()
