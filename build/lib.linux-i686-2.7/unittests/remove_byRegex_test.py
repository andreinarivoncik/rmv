from __future__ import absolute_import
import unittest
import os
import rm.rm_functions
from shutil import rmtree


class RemoveByRegex(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        print ("Remove by regex file or directory!")

    def test_select_byRegex_directory(self):
        basket_path = "temp/basket"
        folder_path = "temp/folder/info"
        os.makedirs(folder_path)
        file_path1 = "temp/folder/info/testfile1.txt"
        file_path2 = "temp/folder/info/testfile2.txt"
        reg = "[a-z]+[1-3].txt"
        with open(file_path1, 'w'):
            pass
        with open(file_path2, 'w'):
            pass
        res = rm.rm_functions.select_for_regex_removing(folder_path, basket_path, reg)
        self.assertTrue(file_path1 in res, "file was not select!")
        self.assertTrue(file_path2 in res, "file was not select!")
        rmtree("temp/folder")

    def test_remove_byRegex_file(self):
        basket_path = "temp/basket"
        reg = "[a-z]+.txt"
        file_path = "temp/testfile.txt"
        with open(file_path, 'w'):
            pass
        if os.path.exists(file_path):

            self.assertRaises(OSError, rm.rm_functions.select_for_regex_removing, file_path,
                              basket_path, reg, "file was not select!")
            os.remove(file_path)

if __name__ == "__main__":
    unittest.main()
