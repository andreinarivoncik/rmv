from __future__ import absolute_import
import unittest
from rm.rm_functions import create_folder
import os


class CreateBasketTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        print ("basket creating begin")

    def test_create_basket(self):
        path = "temp/basket"
        if not os.path.exists(path):
            create_folder(path)
            self.assertTrue(os.path.exists(path), "Basket was not create!")

    def test_create_basket_with_error(self):
        path = "/bin/basket"
        try:
            create_folder(path)
        except OSError as er:
            self.assertEqual(er.errno, 13, "Basket was create!")


if __name__ == "__main__":
    unittest.main()

