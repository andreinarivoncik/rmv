import logging

l1 = logging.getLogger("log1")
l2 = logging.getLogger("log2")

formatter = logging.Formatter(u'%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s')

#handl = logging.Handler(logging.DEBUG)
#handl.setFormatter(formatter)
#handl.setLevel(logging.DEBUG)

l2.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
ch.setFormatter(formatter)

l2.addHandler(ch)


#l2.setLevel(logging.DEBUG)
#l2.addHandler(handl)



#print logging.getLevelName(logging.ERROR)
# logging.log(logging.ERROR, "Hello, kek, %s - %d", "kolya")
#logging.info("Hello world, %s - %d, %s", "andrei", 150, "1998")

l3 = logging.getLogger("log2")

l2.critical("l2 - debuging")
l3.debug("debuging")