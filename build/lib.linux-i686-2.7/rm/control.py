#!/usr/bin/env python
# coding:utf-8

from rm_functions import *
from clean_policy import *
from datetime import date
import datetime
import logging


def ask_user(mess, arg_i, for_all=None):
    if not arg_i:
        return True
    if for_all is None:
        inp = raw_input(mess + " [Y/N]")
        if inp == 'Y':
            return True
        else:
            return False
    if for_all is not None:
        if for_all["for_all"]:
            return True
        else:
            inp = raw_input(mess + " [Y/N/ALL]")
            if inp == "ALL":
                for_all["for_all"] = 1
                return True
            if inp == 'Y':
                return True
            else:
                return False


def get_logger(silent):
    logger = logging.getLogger("logger")
    if silent:
        logger.setLevel(logging.CRITICAL)
    else:
        logger.setLevel(logging.DEBUG)
    return logger


def is_dir_exists(path):
    if not os.path.exists(path):
        return False
    else:
        return True


def check_free_space(path, cur_basket_volume, max_basket_volume):
    if get_size(path) + cur_basket_volume > max_basket_volume:
        return False
    else:
        return True


def remove_control(mode_rm, path, basket_path, silent, arg_i, dry_run, max_basket_volume,
                   user_file_info, json_file_info, other_info_file, regex=None):
    for_all_flag = {"for_all": 0}
    other_info = read_info_json(other_info_file)
    json_format_content = read_info_json(json_file_info)
    logger = get_logger(silent)
    if not is_dir_exists(basket_path):
        raise UserWarning("{}: file or directory not found!".format(basket_path))
    if not is_dir_exists(path):
        raise UserWarning("{}: file or directory not found!".format(path))
    if not check_free_space(path, other_info["cur_baskets_volume"], max_basket_volume):
        raise UserWarning("Memory limit exceeded. Increase the size of the basket or clean it.")

    def write_basket_content(users_format_cont, json_format_cont):
        try:
            append_info_user(user_file_info, users_format_cont)
            write_info_json(json_file_info, json_format_cont)
        except OSError as ex:
            logger.debug("%s, %s: " + ex.message, json_file_info, user_file_info)

    def common_removing(remove_path, in_basket_path, removing_method, for_all=None):
        if not os.access(remove_path, os.W_OK):
            logger.warning("%s: attempt to delete from protected file or directory!", remove_path)
        if ask_user(u"Do you want to remove {}?".format(remove_path), arg_i, for_all):
            if not dry_run:
                try:
                    name = removing_method(remove_path, in_basket_path)
                except OSError as ex:
                    logger.error("%s: " + ex.message, remove_path)
                else:
                    logger.info(u"%s: was successfully deleted!", remove_path)
                    return name
            else:
                logger.info(u"%s: was successfully deleted!", remove_path)
        else:
            logger.info("{}: the operation was successfully canceled!".format(remove_path))
            return None

    if mode_rm == "del":
        new_name = common_removing(path, basket_path, remove)
        if new_name is not None:
            other_info["cur_baskets_volume"] += get_size(os.path.join(basket_path, new_name))
            other_info["cur_objects_count"] += get_count_files_and_directories(os.path.join(basket_path, new_name))
            users_format_content = new_name + '|' + path + '\n'
            json_format_content[new_name] = path
            write_basket_content(users_format_content, json_format_content)

    if mode_rm == "reg":
        if os.path.islink(path) or not os.path.isdir(path):
            raise UserWarning("{}: not directory!".format(path))
        selected_paths = select_for_regex_removing(path, basket_path, regex)
        new_names_and_old_paths = dict()
        for pth in selected_paths:
            new_name = common_removing(pth, basket_path, remove, for_all_flag)
            if new_name is not None:
                other_info["cur_baskets_volume"] += get_size(os.path.join(basket_path, new_name))
                other_info["cur_objects_count"] += get_count_files_and_directories(os.path.join(basket_path, new_name))
                new_names_and_old_paths[new_name] = pth
            if len(new_names_and_old_paths) > 0:
                lines = map(lambda x: "{x}|{y}\n".format(x=x, y=new_names_and_old_paths[x]),
                            new_names_and_old_paths.keys())
                for k, v in new_names_and_old_paths.items():
                    json_format_content[k] = v
                write_basket_content(lines, json_format_content)

    if mode_rm == "recurs":
        if os.path.islink(path) or not os.path.isdir(path):
            raise UserWarning(u"{}: file can not be removed!".format(path))
        selected_paths = select_for_recurs_removing(path)
        selected_paths.remove("")
        dir_path, dir_name = os.path.split(path)
        new_name_for_file_or_directory_in_basket = check_for_new_name(os.path.join(basket_path, dir_name))
        os.rename(path, os.path.join(dir_path, new_name_for_file_or_directory_in_basket))
        for pth in selected_paths:
            from_dir_path = os.path.join(os.path.join(dir_path, new_name_for_file_or_directory_in_basket), pth)
            in_dir_path = os.path.join(os.path.join(basket_path, new_name_for_file_or_directory_in_basket), pth)
            if os.path.exists(from_dir_path):
                is_del = True
            else:
                is_del = False
            if is_del:
                common_removing(from_dir_path, in_dir_path, recurs_remove, for_all_flag)
            if os.path.exists(in_dir_path):
                other_info["cur_baskets_volume"] += get_size(in_dir_path)
                other_info["cur_objects_count"] += get_count_files_and_directories(in_dir_path)
        users_format_content = new_name_for_file_or_directory_in_basket + '|' + path + '\n'
        json_format_content[new_name_for_file_or_directory_in_basket] = path
        write_basket_content(users_format_content, json_format_content)
        if os.path.exists(os.path.join(dir_path, new_name_for_file_or_directory_in_basket)):
            os.rename(os.path.join(dir_path, new_name_for_file_or_directory_in_basket), path)
    write_info_json(other_info_file, other_info)
    return 0


def recovery_control(name, basket_path, silent, dry_run, arg_i, user_file_info, json_file_info):
    logger = get_logger(silent)
    if not ask_user("Do you want to recovery file or directory?", arg_i):
        logger.info("The recovery operation was successfully canceled!")
        return 0
    find = False
    lines_list = read_info_user(user_file_info)
    lines_dict = read_info_json(json_file_info)
    line = filter(lambda x: x.split('|')[0] == name, lines_list)
    if line:
        find = True
        n, p = line[0].split('|')
        p = p[:len(p)-1]
        if os.path.exists(p):
            logger.warning(u"%s: file or directory with that name already exists!", p)
            if not ask_user("Replace file?", arg_i):
                while os.path.exists(p):
                    p += ' new'
            else:
                if not dry_run:
                    os.remove(p)
                logger.info("file %s successfully changed!", p)
        if not dry_run:
            lines_list.remove(line[0])
            lines_dict.pop(n)
            write_info_user(user_file_info, lines_list)
            write_info_json(json_file_info, lines_dict)
            recovery(os.path.join(basket_path, n), p)
        logger.info("file %s was successfully recovered!", name)
    if not find:
        raise UserWarning("{}: file or directory not found!".format(os.path.join(basket_path, name)))
    return 0


def basket_cleaning_control(cleaning_policy, silent, dry_run, arg_i,
                            user_basket_content_file, json_basket_content_file, other_info_file):
    logger = get_logger(silent)
    other_info = read_info_json(other_info_file)

    def update_info(new_baskets_content, cleaned_files_count, cleaned_files_size):
        other_info["last_cleaning"] = str(date.today())
        other_info["cur_baskets_volume"] -= cleaned_files_size
        other_info["cur_objects_count"] -= cleaned_files_count
        write_info_json(other_info_file, other_info)
        if os.path.exists(json_basket_content_file):
            with open(json_basket_content_file, 'w'):
                write_info_json(json_basket_content_file, new_baskets_content)
                logger.debug("%s: file was updated!!", json_basket_content_file)
        else:
            logger.debug("%s: file corrupt or deleted!", json_basket_content_file)
            with open(json_basket_content_file, 'w'):
                write_info_json(json_basket_content_file, dict())
        if os.path.exists(user_basket_content_file):
            with open(user_basket_content_file, 'w'):
                new_baskets_content = map(lambda x: str(x[0])+'|'+str(x[1]+'\n'), new_baskets_content.items())
                write_info_user(user_basket_content_file, new_baskets_content)
                logger.debug("%s: file was updated!", user_basket_content_file)
        else:
            logger.debug("%s: file corrupt or deleted!", user_basket_content_file)
            with open(user_basket_content_file, 'w'):
                pass

    if ask_user("Do you want to clear a basket?", arg_i):
        count = 0
        size = 0
        removing_list = cleaning_policy.get_paths_for_cleaning()
        baskets_content = read_info_json(json_basket_content_file)
        for path in removing_list:
            size += get_size(path)
            if not dry_run:
                count += clean_dir(path)
                baskets_content.pop(os.path.split(path)[1])
        if not dry_run:
            update_info(baskets_content, count, size)
        logger.info("Basket cleared successfully! Was cleared %s kB.", size)
        return 0
    else:
        logger.info("The operation was successfully canceled!")
        return 0


def view_basket(basket_path, args_i, view_count):
    logger = get_logger(False)
    for_all_flag = {"for_all": 0}
    if not ask_user("Do you want to view basket?", args_i):
        logger.info("The view operation was successfully canceled!")
        return 0
    count = view_count
    for f in os.listdir(basket_path):
        try:
            logger.info(u"Name: {name},\tvolume: {volume},\tremoved date: {date}".
                        format(name=f, volume=os.stat(os.path.join(basket_path, f)).st_size,
                               date=datetime.datetime.fromtimestamp(os.stat(os.path.join(basket_path, f)).st_mtime)))
            count -= 1
        except OSError as er:
            logger.warning(u"{name}: {message} {text}".format(name=f, message=er.strerror, text="Corrupted link!"))
        if count == 0:
            if ask_user("can you give next files?", args_i, for_all_flag):
                count = view_count
            else:
                break
    logger.info("Successful basket view!")
    return 0
