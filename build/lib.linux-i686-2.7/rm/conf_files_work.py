#!/usr/bin/env python
# coding:utf-8

import os
import json
import ConfigParser


# logger = logging.getLogger(__name__)
def read_config_json_file(conf_file):
    if os.path.exists(conf_file):
        with open(conf_file, 'r') as fp:
            conf_object = json.load(fp, cls=None, object_hook=None, parse_float=None,
                                    parse_int=None, parse_constant=None, object_pairs_hook=None)
        return conf_object


def write_config_json_file(conf_file, conf_object):
    if os.path.exists(conf_file):
        with open(conf_file, 'w') as fp:
            json.dump(conf_object, fp, skipkeys=False, ensure_ascii=True, check_circular=True, allow_nan=True,
                      cls=None, indent=4, separators=None, default=None, sort_keys=False)


def read_config_info_user(user_file):
    if os.path.exists(user_file):
        with open(user_file) as f:
            return f.readlines()


def write_config_info_user(user_file, st):
    if os.path.exists(user_file):
        with open(user_file, 'w') as f:
            f.writelines(st)


def append_config_info_user(user_file, st):
    if os.path.exists(user_file):
        with open(user_file, 'a') as f:
            f.writelines(st)


def read_config_file(conf_file):
    conf_pars = ConfigParser.RawConfigParser()
    conf_pars.read(conf_file)
    return conf_pars


def write_config_file(conf_file, conf_parser):
    with (open(conf_file, 'w')) as f:
        conf_parser.write(f)


def read_conf_path():
    with open("/tmp/rmv_conf", 'r') as f:
        conf_path = f.readline()
        conf_path = conf_path[:len(conf_path) - 1]
    return conf_path
