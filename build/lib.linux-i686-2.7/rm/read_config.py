#!/usr/bin/python
# ! -*- coding: utf-8 -*-

from conf_files_work import read_config_json_file
import logging
import os


def read_configuration(args, default_conf, conf):
    logger = logging.getLogger("logger")
    silent = args["silent"]
    if silent:
        logger.setLevel(logging.CRITICAL)
    else:
        logger.setLevel(logging.INFO)
    if args["config"] is not None:
        if "configuration=conf1" in args["config"]:
            conf_obj = read_config_json_file(conf + "conf1.json")
        elif "configuration=conf2" in args["config"]:
            conf_obj = read_config_json_file(conf + "conf2.json")
        else:
            conf_obj = read_config_json_file(default_conf)
    else:
        conf_obj = read_config_json_file(default_conf)
    configs = change_configuration(args, conf_obj)
    return configs


def change_configuration(args, configs):
    logger = logging.getLogger("logger")
    silent = args["silent"]
    if silent:
        logger.setLevel(logging.CRITICAL)
    else:
        logger.setLevel(logging.INFO)

    if args["config"] is None:
        return configs
    for setting in args["config"]:
        try:
            k, v = setting.split('=')
        except ValueError:
            logger.warning("undefined argument %s", setting)
        else:
            if k == "path":
                if os.path.exists(os.path.expanduser(v)):
                    configs[k] = v
                else:
                    logger.info("the basket path did not changed!")
            elif k == "manual_cleaning_politic":
                if v == "max_volume" or v == "time_interval" or v == "mix_cleaning":
                    configs[k] = v
                else:
                    logger.info("wrong manual_cleaning_politic!")
            else:
                try:
                    val = int(v)
                except ValueError:
                    logger.error("wrong setting: %s", setting)
                else:
                    if k == "view_count" and val > 0:
                        configs[k] = val
                    if k == "max_basket_volume":
                        if val <= 0:
                            logger.info("wrong max_basket_volume!")
                        else:
                            configs[k] = val
                    if k == "days" and val > 0:
                        configs[k] = val
                    if k == "volume":
                        if 0 < val < configs["max_basket_volume"]:
                            configs[k] = val
                        else:
                            logger.info("volume: wrong argument!")
    return configs
