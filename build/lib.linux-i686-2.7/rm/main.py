#!/usr/bin/python
# ! -*- coding: utf-8 -*-

import log
import re
from conf_files_work import write_config_info_user
from conf_files_work import read_conf_path
from conf_files_work import write_config_json_file
from parser_constructor import pars_init
from read_config import *
from control import *
from rm_functions import *


def create_loggers(config_path):
    logs_file = "{}fi/log.log".format(config_path)
    log.create_logger("logger", log.logging.DEBUG,
                      file_log_format=u'%(filename)s[LINE:%(lineno)d]# %(levelname)-5s [%(asctime)s]  %(message)s',
                      log_format=u'%(levelname)-5s %(message)s', filemode='a', filename=logs_file)


def create_basket(configs):
    logger = logging.getLogger("logger")
    basket_path = os.path.expanduser(configs["path"] + "/basket")
    if not os.path.exists(basket_path):
        create_folder(basket_path)
        logger.info("basket was successfully created!")
    return 0


def show_basket(args, configs):
    silent = args["silent"]
    logger = get_logger(silent)
    basket_path = os.path.expanduser(configs["path"] + "basket")
    if args["view"]:
        try:
            return view_basket(basket_path, args['i'], configs["view_count"])
        except OSError as er:
            logger.error("baskets showing: %s", er.message)
            return -2
        except Exception as ex:
            logger.error("baskets showing: %s", ex.message)
            return -2


def run_removing(args, configs, config_path):
    silent = args["silent"]
    logger = get_logger(silent)
    user_format_file = "{}fi/basket_content.txt".format(config_path)
    json_format_file = "{}fi/basket_content.json".format(config_path)
    other_info_file = "{}fi/other_info.json".format(config_path)
    basket_path = os.path.expanduser(configs["path"] + "basket")

    def check_basket_and_file_in_basket(path, basket__path):
        if basket__path == path:
            logger.info("Basket can not be deleted!")
            return False
        if re.search(basket__path, path):
            logger.info("{}: file is already in the basket!".format(args['del']))
            return False
        return True

    res = 0
    for p in args["path"]:
        p = os.path.join(os.path.abspath(os.path.curdir), p)
        checking = check_basket_and_file_in_basket(p, basket_path)
        try:
            if args["del"] and checking:
                res = remove_control("del", p, basket_path, silent, args['i'], args["dry_run"],
                                     configs["max_basket_volume"], user_format_file, json_format_file, other_info_file)
            elif args["recurs"] and checking:
                res = remove_control("recurs", p, basket_path, silent, args['i'], args["dry_run"],
                                     configs["max_basket_volume"], user_format_file, json_format_file, other_info_file)
            elif args["reg"] and checking:
                res = remove_control("reg", p, basket_path, silent, args['i'], args["dry_run"],
                                     configs["max_basket_volume"], user_format_file,
                                     json_format_file, other_info_file, args["reg"])
        except UserWarning as warn:
            logger.warning("removing: %s", warn.message)
            if not args['f']:
                return -3
        except Exception as ex:
            logger.warning("removing: %s", ex.message)
            if not args['f']:
                return -3
    return res


def run_basket_cleaning(args, configs, config_path):
    arg_i = args['i']
    dry_run = args["dry_run"]
    silent = args["silent"]
    logger = get_logger(silent)
    basket_path = os.path.expanduser(configs["path"] + "basket")
    user_basket_content_file = "{}fi/basket_content.txt".format(config_path)
    json_basket_content_file = "{}fi/basket_content.json".format(config_path)
    other_info_file = "{}fi/other_info.json".format(config_path)
    logs_file = "{}fi/log.log".format(config_path)
    clean_policy = None
    with open(logs_file, 'w'):
        pass
    if configs["cleaning_policy"] == "full_cleaning":
        clean_policy = FullCleaning(basket_path)
    if configs["cleaning_policy"] == "mix_cleaning":
        clean_policy = MixCleaning(basket_path, configs["dir_amount"], configs["days"])
    if configs["cleaning_policy"] == "time_interval":
        clean_policy = TimeInterval(basket_path, configs["days"])
    if configs["cleaning_policy"] == "max_amount":
        clean_policy = MaxAmount(basket_path, configs["dir_amount"])
    try:
        return basket_cleaning_control(clean_policy, silent, dry_run, arg_i, user_basket_content_file,
                                       json_basket_content_file, other_info_file)
    except OSError as er:
        logger.exception("baskets cleaning: %s", er.message)
        return -4
    except Exception as ex:
        logger.exception("baskets cleaning: %s", ex.message)
        return -4


def run_recovery(name, args, configs, config_path):
    silent = args["silent"]
    dry_run = args["dry_run"]
    arg_i = args['i']
    logger = get_logger(silent)
    user_format_file = "{}fi/basket_content.txt".format(config_path)
    json_format_file = "{}fi/basket_content.json".format(config_path)
    basket_path = os.path.expanduser(configs["path"] + "basket")
    try:
        return recovery_control(name, basket_path, silent, dry_run, arg_i, user_format_file, json_format_file)
    except UserWarning as warn:
        logger.error("baskets recovering: %s", warn.message)
        return 0
    except OSError as er:
        logger.error("baskets recovering: %s", er.message)
        return -5
    except Exception as ex:
        logger.error("baskets recovering: %s", ex.message)
        return -5


def write_update_config(configs, config_path):
    logger = get_logger("logger")
    user_format_file = "{}cf/default_user_config.txt".format(config_path)
    json_format_file = "{}cf/default_JSON_config.json".format(config_path)
    try:
        write_config_json_file(json_format_file, configs)
        write_config_info_user(user_format_file, [str(k) + '|' + str(v) + '\n' for (k, v) in configs.items()])
    except OSError as er:
        logger.debug("configuration files writing: %s", er.strerror)
        return -6
    return 0


def main():
    args = vars(pars_init().parse_args())
    config_path = read_conf_path()
    create_loggers(config_path=config_path)

    # Change configuration by command line parameters
    configs = read_configuration(args, default_conf="{}cf/default_JSON_config.json".format(config_path),
                                 conf="{}cf/".format(config_path))
    # Create a basket
    res = create_basket(configs)
    if not res == 0:
        return res

    # Cleaning of the basket in case it's time or the user wanted
    if args["clear"]:
        res = run_basket_cleaning(args, configs, config_path)
        if not res == 0:
            return res

    # Delete depending on the delete settings
    if args["recurs"] or args["reg"] or args["del"]:
        res = run_removing(args, configs, config_path)
        if not res == 0:
            return res

    # Recovery files or directories
    if args["rec"]:
        res = run_recovery(args["rec"], args, configs, config_path)
        if not res == 0:
            return res

    # Show basket
    if args["view"]:
        res = show_basket(args, configs)
        if not res == 0:
            return res

    # Write configuration changes in file
    res = write_update_config(configs, config_path)
    return res

if __name__ == "__main__":
    lg = logging.getLogger("logger")
    lg.setLevel(logging.INFO)
    lg.info("Process finished with exit code %s", str(main()))
