#!/usr/bin/python
# ! -*- coding: utf-8 -*-

from datetime import date


class ClearPolitic:
    def __init__(self, args, config):
        self.args = args
        self.config = config

    def get_politic(self):
        if self.config["manual_cleaning_politic"] == "mix_cleaning":
            return self.max_volume_cleaning(self.config["volume"], self.config["cur_basket_volume"])
        if self.config["manual_cleaning_politic"] == "time_interval":
            return self.time_interval_cleaning(self.config["last_cleaning"], self.config["days"])
        if self.config["manual_cleaning_politic"] == "max_volume":
            return self.mix_cleaning(self.config["last_cleaning"], self.config["days"],
                                     self.config["volume"], self.config["cur_basket_volume"])

    @staticmethod
    def max_volume_cleaning(volume, cur_volume):
        if cur_volume > volume:
            return True
        else:
            return False

    @staticmethod
    def time_interval_cleaning(last_cleaning, days):
        year, month, day = map(int, last_cleaning.split('-'))
        if (date.today() - date(year, month, day)).days >= days:
            return True
        else:
            return False

    @staticmethod
    def mix_cleaning(last_cleaning, days, volume, cur_volume):
        year, month, day = map(int, last_cleaning.split('-'))
        if (date.today() - date(year, month, day)).days >= days and cur_volume > volume:
            return True
        else:
            return False
