from __future__ import absolute_import
import unittest
import os
import rm.conf_files_work
import rm.read_config


class ReadConfClass(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        print ("Read configuration")

    def test_read_config(self):
        path = "../cf/default_json_config.json"
        if os.path.exists(path):
            config_object = rm.conf_files_work.read_config_json_file(path)
            config_object_keys = config_object.keys()
            self.assertIn("view_count", config_object_keys, "view count not found!")
            self.assertIn("max_basket_volume", config_object_keys, "max_basket_volume not found!")
            self.assertIn("days", config_object_keys, "days not found!")
            self.assertIn("volume", config_object_keys, "volume not found!")
            self.assertIn("last_cleaning", config_object_keys, "last_cleaning not found!")
            self.assertIn("cur_basket_volume", config_object_keys, "cur_basket not found!")
            self.assertIn("for_all", config_object_keys, "for_all not found!")
            self.assertIn("manual_cleaning_politic", config_object_keys, "manual_cleaning_politic not found!")

    def test_check_type_config(self):
        path = "../cf/default_JSON_config.json"
        if os.path.exists(path):
            config_object_type = type(rm.conf_files_work.read_config_json_file(path))
            self.assertTrue(config_object_type is dict, "Objects type is not dict!")


if __name__ == "__main__":
    unittest.main()
