from __future__ import absolute_import
import unittest
import os
import shutil
import rm.rm_functions


class ClearBasketTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        print ("Clear basket")

    @classmethod
    def tearDownClass(cls):
        basket_path = "temp/basket"
        if os.path.exists(basket_path):
            shutil.rmtree(basket_path)

    def test_clear_basket(self):
        path = "temp/basket"
        os.makedirs("temp/basket/temp/test_folder1")
        os.mkdir("temp/basket/123.txt")
        os.mkdir("temp/basket/321")
        rm.rm_functions.full_basket_cleaning(path)
        self.assertEqual(len(os.listdir(path)), 0, "Basket was not clear!")

if __name__ == "__main__":
    unittest.main()
