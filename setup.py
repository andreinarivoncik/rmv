#!/usr/bin/python
# ! -*- coding: utf-8 -*-

from os import path
import logging


def install():
    logging.basicConfig(level=logging.INFO)
    if path.exists(path.join(path.expanduser('~'), '.bashrc')):
        fr = open(path.join(path.expanduser('~'), '.bashrc'), 'r')
        if not "alias rmv='python " + (path.join(path.abspath(path.curdir), "rm/main.py") + "'\n") in fr.readlines():
            fr.close()
            with open(path.join(path.expanduser('~'), '.bashrc'), 'a') as f:
                f.write("alias rmv='python " + (path.join(path.abspath(path.curdir), "rm/main.py") + "'\n"))
            with open("/tmp/rmv_conf", 'w') as f:
                f.write(path.abspath(path.curdir) + '/' + "\n")
            logging.info("Installation completed successfully!")
        else:
            fr.close()
            logging.info("The program is already installed!")
    else:
        logging.error("Installation error!")


if __name__ == '__main__':
    install()


